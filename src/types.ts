class VueTest {
    idx: number;
    constructor() {
        this.idx = 10;
    }
    add(value:number) {
        const prevValue = value;
        this.idx += value;
        return `prev:${prevValue}, current:${this.idx}`;
    }
}

//let test = new VueTest();
//console.log(`types:${test.add(10)}`)

export default VueTest;
