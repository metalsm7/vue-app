import Vue from 'vue';
import App from './app.vue';
import VueTest from './types.ts';
import './style/style.scss';

let vt = new VueTest();
vt.add(10);

const app1 = new Vue({
  el: '#app',
  data: {
    message: 'hello.js에서 실행!' + vt.add(10)
  }
});

const app2 = new Vue({
  el: '#app',
  components: { 'app-comp': App }
});
