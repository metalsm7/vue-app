var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Hello World2!' });
});
router.get('/hello/:name', (req, res, next) => {
  res.render('hello', { name: req.params.name });
});

module.exports = router;
